@component('mail::message')
# Confirmar Email

Por favor verificar su email

@component('mail::button', ['url' => 'http://localhost:8000/{{$token}}'])
Verificar
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
