<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|string',
            'lastname'    => 'required|string',
            'phone'       => 'required|numeric',
            'country'     => 'required|string',
            'birthday'    => 'required',
            'email'       => 'required|email|unique:users',
            'password'    => 'required',
            're_password' => 'required|same:password',
        ];
    }
}
