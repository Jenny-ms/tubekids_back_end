<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Http\Requests\ProfileRequest;
use Response;

class ProfileController extends Controller
{
    public function index()
    {
        $token    = request()->bearerToken();
        $user     = auth()->user();
        $profiles = Profile::where('user_id', $user->id)->get();
        return $profiles;
    }
    public function store(ProfileRequest $request)
    {
        $user = auth()->user();
        $info = array ('user_name' => $request->user_name,'full_name' => $request->full_name,'age' => $request->age,'pin' => $request->pin, 'user_id' => $user->id);
        $profile = Profile::create($info);
        $response = Response::make(json_encode(['data'=>$profile]), 201)->header('Location', 'http://localhost:8000/api/profiles/'.$profile->id)->header('Content-Type', 'application/json');
        return $response;
    }
    public function destroy($id)
    {
        $profile = Profile::find($id);
        if (!$profile) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se ha encontrado el profile.'])], 404);
        }
        $profile->delete();
        return response()->json(null, 204);
    }
    public function show($id)
    {
        $profile = Profile::find($id);
        if (!$profile) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se ha encontrado el profile.'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$profile], 200);
    }
    public function update(ProfileRequest $request)
    {
        $profile = profile::find($request->id)->update($request->all());
        return response()->json(['status'=>'ok','data' => $profile], 200);
    }
}
