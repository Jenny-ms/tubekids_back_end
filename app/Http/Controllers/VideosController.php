<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Videos;
use Illuminate\Support\Facades\Storage;
use Response;

class VideosController extends Controller
{
    public function index()
    {
        $user   = auth()->user();
        $videos = Videos::where('user_id', $user->id)->get();
        return $videos;
    }
    public function destroy($id)
    {
        $video = Videos::find($id);
        if (!$video) {
            return response()->json(['errors'=>array(['message'=>'No se ha encontrado el video','code'=> 404])], 404);
        }
        $video->delete();
        return response()->json(204);
    }
    public function show($id)
    {
        $video = Videos::find($id);
        if (!$video) {
            return response()->json(['errors'=>array(['code'=> 404, 'message'=>'No se ha encontrado el video.'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$video], 200);
    }

    public function store(Request $request)
    {
        $user   = auth()->user();

        $video = array(
            'name'    => $request->name,
            'type'    => $request->type,
            'url'     => $request->url,
            'user_id' => $user->id,
        );
        if ($request->file('video')) {
            $path = Storage::disk('public')->put('listVideos', $request->file('video'));
            $video['url'] = asset($path);
        }

        $newVideo = Videos::create($video);
        $response = Response::make(json_encode(['data'=>$newVideo ]), 201)->header('Location', 'http://localhost/api/videos/'.$newVideo ->id)->header('Content-Type', 'application/json');
        return $response;
    }
    public function update(Request $request)
    {
        $user   = auth()->user();

        $video = array(
            'id'      => $request->id,
            'name'    => $request->name,
            'type'    => $request->type,
            'url'     => $request->url,
            'user_id' => $user->id,
        );
        if ($request->file('video')) {
            $path = Storage::disk('public')->put('listVideos', $request->file('video'));
            $video['url'] = asset($path);
        }

        $video = Videos::find($request->id)->update($video);
        return response()->json(['status'=>'ok','data' => $video], 200);
    }
}
