<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Symfony\Component\HttpFoundation\Response;

class CodeController extends Controller
{
    public function codemessage(Request $request)
    {
        if ($user = User::where('code', $request->code)->where('email', $request->email)->first()) {
            $user->active = true;
            $user->code  = null;
            $user->save();
            return response()->json([
            'data' => 'Your account is actived.'], Response::HTTP_OK);
        }
        return response()->json([
            'error' => 'Please try again, code is not correct'
        ], Response::HTTP_NOT_FOUND);
    }

    public function send($email)
    {
        $user = User::where('email', $email)->first();
        $user->code = $this->code($user->phone);
        $user->active = false;
        if ($user->save()) {
            return response()->json([
                'data' => 'Your code has been sending'
            ], Response::HTTP_OK);
        }
        return response()->json([
            'error' => 'Your code was not sending'
        ], Response::HTTP_NOT_FOUND);
    }
    public function code($phone)
    {
        $code  = rand(1111, 9999);
        $nexmo = app('Nexmo\Client');
        $nexmo->message()->send([
            'to'   => '+506'. (int) $phone,
            'from' => 'Nexmo',
            'text' => 'Verify code: ' . $code,
        ]);
        return $code;
    }
}
