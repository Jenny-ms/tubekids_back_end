<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
         'user_name', 'full_name', 'pin', 'age', 'user_id',
    ];
}
