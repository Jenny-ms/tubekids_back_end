<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::resource('videos', 'VideosController');
    Route::post('videos/{id}', 'VideosController@update');
    Route::resource('profiles', 'ProfileController');
    Route::post('me', 'AuthController@me');
    Route::post('verify/email', 'EmailController@sendEmail')->name('verify.email');
    Route::get('confirm/email/{token}', 'EmailController@confirmEmail')->name('confirm.email');

});